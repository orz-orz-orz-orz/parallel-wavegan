#!/bin/bash

# Copyright 2019 Tomoki Hayashi
#  MIT License (https://opensource.org/licenses/MIT)

download_dir=$1

# check arguments
if [ $# != 1 ]; then
    echo "Usage: $0 <download_dir>"
    exit 1
fi

speakers=("slt" "bdl" "jmk")

set -euo pipefail

# download datasets
cwd=$(pwd)

# note:  "${speakers[@]}" is equivalent to "slt" "bdl" "jmk"
# be careful ${speaker[@]} and "${speakers[@]}" have different results.
for spk in "${speakers[@]}"
do
    if [ ! -e "${download_dir}/cmu_us_${spk}_arctic" ]; then
        echo "Downloading ${spk}."
        mkdir -p "${download_dir}"
        cd "${download_dir}"
        wget "http://festvox.org/cmu_arctic/cmu_arctic/orig/cmu_us_${spk}_arctic-WAVEGG.tar.bz2"
        tar xf "cmu_us_${spk}_arctic-WAVEGG.tar.bz2"
        rm "cmu_us_${spk}_arctic-WAVEGG.tar.bz2"
        cd "${cwd}"
       
    else
        echo "Already exists. Skip download ${spk}."
    fi
done